# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"POT-Creation-Date: 2019-08-09 14:16+0000\n"
"PO-Revision-Date: 2016-06-04 14:53+0200\n"
"Last-Translator: Tails translators <tails@boum.org>\n"
"Language-Team: Tails Translators <tails-l10n@boum.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"

#. type: Content of: outside any tag (error?)
msgid "[[!meta title=\"Write to our help desk\"]]"
msgstr ""

#. type: Content of: <div><h3>
msgid "Email"
msgstr ""

#. type: Content of: <div>
msgid "[[!img lib/email.png link=no]]"
msgstr ""

#. type: Content of: <div><p>
msgid "You can write to our help desk by email:"
msgstr ""

#. type: Content of: <div><p>
msgid "[[tails-support-private@boum.org|about/contact#tails-support-private]]"
msgstr ""

#. type: Content of: <div>
msgid "[[!inline pages=\"support/talk/languages.inline\" raw=\"yes\"]]"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Encrypting your emails with our [[OpenPGP key|doc/about/"
"openpgp_keys#support]] is the only way to achieve end-to-end encryption."
msgstr ""

#. type: Content of: <div><h3>
msgid "Chat"
msgstr ""

#. type: Content of: <div>
msgid "[[!img lib/chat.png link=no]]"
msgstr ""

#. type: Content of: <div><p>
msgid "Join our XMPP chat room to talk to contributors and users."
msgstr ""

#. type: Content of: <div><ul><li>
msgid "server: <code>conference.riseup.net</code>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "room: <code>tails</code>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "use TLS/SSL to connect!"
msgstr ""
